//
//  FontManager.swift
//  PTEMaster
//
//  Created by mac on 17/08/20.
//  Copyright © 2020 CTIMac. All rights reserved.
//

import Foundation
import UIKit

let FontRegular = "Poppins-Regular"
let semiBold = "Poppins-SemiBold"
let FontBold = "Poppins-Bold"


let Thin = "Poppins-Thin"
let Medium = "Poppins-Medium"
let Light = "Poppins-Light"



enum Fonts : Int {
    
    case normal = 1
    case semibold = 2
    case bold = 3
    
    case Thin = 11
    case  Light = 22
    case  Medium = 33
    
    public func font(WithSize size : CGFloat) -> UIFont {
        switch self {
        case .normal:
           return UIFont.regularFont(size: size)
        case .bold:
            return UIFont.boldFont(size: size)
        case .semibold:
            return UIFont.semiboldFont(size: size)
        case .Thin:
            return UIFont.thinFont(size: size)
        case .Light:
            return UIFont.lightFont(size: size)
        case .Medium:
            return UIFont.mediumFont(size: size)
        }
    }
}

protocol CustomisableOutlet {
    var custom_font : Int { get set }
    var rds_widthPerc : CGFloat { get set }
    var rds_heightPerc : CGFloat { get set }
}

extension CustomisableOutlet where Self: UIView {
    func setRadiusIfNeeded() {
        if self.rds_widthPerc != 0 {
            self.layer.cornerRadius = self.bounds.width * rds_widthPerc/100
        } else if self.rds_heightPerc != 0 {
            self.layer.cornerRadius = self.bounds.height * rds_heightPerc/100
        }
    }
}


class CustomLabel: UILabel, CustomisableOutlet {
    
    @IBInspectable var rds_widthPerc: CGFloat = 0.0
    
    @IBInspectable var rds_heightPerc: CGFloat = 0.0
    
    @IBInspectable var custom_font : Int = 0 {
        didSet {
            self.font = Fonts.init(rawValue: custom_font)?.font(WithSize: self.font.pointSize)
        }
    }
   
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.setRadiusIfNeeded()
    }
}


class CustomTextfield: UITextField, CustomisableOutlet {
    
    @IBInspectable var rds_widthPerc: CGFloat = 0.0
    
    @IBInspectable var rds_heightPerc: CGFloat = 0.0
    
    @IBInspectable var custom_font : Int = 0 {
        didSet {
            self.font = Fonts.init(rawValue: custom_font)?.font(WithSize: self.font?.pointSize ?? 0)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.setRadiusIfNeeded()
    }
}

class CustomButton: UIButton, CustomisableOutlet {
    
    @IBInspectable var rds_widthPerc: CGFloat = 0.0
    
    @IBInspectable var rds_heightPerc: CGFloat = 0.0
    
    @IBInspectable var custom_font : Int = 0 {
        didSet {
            self.titleLabel?.font = Fonts.init(rawValue: custom_font)?.font(WithSize: self.titleLabel?.font?.pointSize ?? 0)
        }
    }//button.titleLabel!.font
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.setRadiusIfNeeded()
    }
}

extension UIFont {
    
    static func regularFont( size:CGFloat ) -> UIFont{
        return  UIFont(name: FontRegular , size: size)!
    }
    
    static func boldFont( size:CGFloat ) -> UIFont{
        return  UIFont(name: FontBold , size: size)!
    }
    
    static func semiboldFont( size:CGFloat ) -> UIFont{
        return  UIFont(name: semiBold , size: size)!
    }
    
    static func thinFont( size:CGFloat ) -> UIFont{
        return  UIFont(name: Thin , size: size)!
    }
    
    static func mediumFont( size:CGFloat ) -> UIFont{
        return  UIFont(name: Medium , size: size)!
    }
    static func lightFont( size:CGFloat ) -> UIFont{
        return  UIFont(name: Light , size: size)!
    }
    
    
}



