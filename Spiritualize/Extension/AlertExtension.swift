//
//  AlertExtension.swift
//  Auction
//
//  Created by mac on 10/12/20.
//

import Foundation
import UIKit
class Util: NSObject {
    // MARK: - Alert Methods
    class func showNetWorkAlert()
    {
        showAlertWithCallback("No Network Connection", message: "Please check your connection and try again.", isWithCancel: false)
        // Loader.hideLoader()
    }
    
    //MARK:- alert with handler
    class func showAlertWithCallback(_ title: String?, message: String?, isWithCancel: Bool, handler: (() -> Void)? = nil) {
        if UIApplication.topViewController() != nil {
            if (UIApplication.topViewController()!.isKind(of: UIAlertController.self)) {
                return
            }
        }
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if isWithCancel {
            alertController.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        }
        
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            handler?()
        }))
        appDelegate.window?.rootViewController?.present(alertController, animated: true, completion: nil)
      //  UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    class func showAlertWithCancelCallback(_ title: String?, message: String?, isWithCancel: Bool, handler: ((String) -> Void)? = nil) {
        if UIApplication.topViewController() != nil {
            if (UIApplication.topViewController()!.isKind(of: UIAlertController.self)) {
                return
            }
        }
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            handler?("Ok")
        }))
        
        if isWithCancel {
            alertController.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) in
                handler?("Cancel")
            }))
        }
        
        appDelegate.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    
    
    
    
        class func show(message: String) {
            let controller =    UIApplication.shared.topMostViewController()
            let toastContainer = UIView(frame: CGRect())
            toastContainer.backgroundColor = UIColor(red: 45/255, green: 45/255, blue: 45/255, alpha: 1)
            toastContainer.alpha = 0.0
            toastContainer.layer.cornerRadius = 8;
            toastContainer.clipsToBounds  =  true

            let toastLabel = UILabel(frame: CGRect())
            toastLabel.textColor = UIColor.white
            toastLabel.textAlignment = .center;
            toastLabel.font = UIFont(name: "Poppins-Regular",
                                    size: 15.0)
            toastLabel.text = message
            toastLabel.clipsToBounds  =  true
            toastLabel.numberOfLines = 0

            toastContainer.addSubview(toastLabel)
            controller?.view.addSubview(toastContainer)

            toastLabel.translatesAutoresizingMaskIntoConstraints = false
            toastContainer.translatesAutoresizingMaskIntoConstraints = false

            let a1 = NSLayoutConstraint(item: toastLabel, attribute: .leading, relatedBy: .equal, toItem: toastContainer, attribute: .leading, multiplier: 1, constant: 15)
            let a2 = NSLayoutConstraint(item: toastLabel, attribute: .trailing, relatedBy: .equal, toItem: toastContainer, attribute: .trailing, multiplier: 1, constant: -15)
            let a3 = NSLayoutConstraint(item: toastLabel, attribute: .bottom, relatedBy: .equal, toItem: toastContainer, attribute: .bottom, multiplier: 1, constant: -15)
            let a4 = NSLayoutConstraint(item: toastLabel, attribute: .top, relatedBy: .equal, toItem: toastContainer, attribute: .top, multiplier: 1, constant: 15)
            toastContainer.addConstraints([a1, a2, a3, a4])
            
            
            

            let c1 = NSLayoutConstraint(item: toastContainer, attribute: .leading, relatedBy: .equal, toItem: controller?.view, attribute: .leading, multiplier: 1, constant: 65)
            let c2 = NSLayoutConstraint(item: toastContainer, attribute: .trailing, relatedBy: .equal, toItem: controller?.view, attribute: .trailing, multiplier: 1, constant: -65)
            let c3 = NSLayoutConstraint(item: toastContainer, attribute: .bottom, relatedBy: .equal, toItem: controller?.view, attribute: .bottom, multiplier: 1, constant: -110)
            controller?.view.addConstraints([c1, c2, c3])

            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
                toastContainer.alpha = 1.0
            }, completion: { _ in
                UIView.animate(withDuration: 0.5, delay: 1.5, options: .curveEaseOut, animations: {
                    toastContainer.alpha = 0.0
                }, completion: {_ in
                    toastContainer.removeFromSuperview()
                })
            })
        }
 
    
    
}


//Return top or visible view controller
extension UIApplication {
    
    class func topViewController(_ base: UIViewController? = appDelegate.window?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    }
}



