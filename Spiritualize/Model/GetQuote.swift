//
//  GetQuote.swift
//  Spiritualize
//
//  Created by mac on 21/12/20.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let getQuoteModal = try? newJSONDecoder().decode(GetQuoteModal.self, from: jsonData)

import Foundation



// MARK: - GetQuoteModal
struct GetQuoteModal: Codable {
    var code, message: String?
    var quotes: [Quote]?
}

// MARK: - Quote
struct Quote: Codable {
    var quotesID, quoteDescription, name, image: String?
    var status, created,userID,favourite ,modified: String?
    var  source, fontColor , email: String?

    enum CodingKeys: String, CodingKey {
        case quotesID = "quotes_id"
        case fontColor = "font_color"
        case quoteDescription = "description"
        case name, image, status, created, modified
        case userID = "user_id"
        case email, favourite, source
    }
}
