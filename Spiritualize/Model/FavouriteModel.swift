//
//  Favourite.swift
//  Spiritualize
//
//  Created by mac on 22/12/20.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let favourite = try? newJSONDecoder().decode(Favourite.self, from: jsonData)

import Foundation

// MARK: - FavouriteModel
struct FavouriteModel: Codable {
    var code, message: String?
    var favourite: [Favourite]?
}

// MARK: - Favourite
struct Favourite: Codable {
    var id, fullName, email, password: String?
    var profileImage, deviceType, iosToken, androidToken: String?
    var status, verify, createdAt, token: String?
    var remember, otp, favourite, userID: String?
    var quotes, quotesID, favouriteDescription, name: String?
    var image, created,source, modified,fontColor : String?

    enum CodingKeys: String, CodingKey {
        case id
        case fontColor =  "font_color"
        case fullName = "full_name"
        case email, password
        case profileImage = "profile_image"
        case deviceType = "device_type"
        case iosToken = "ios_token"
        case androidToken = "android_token"
        case status, verify
        case createdAt = "created_at"
        case token, remember, otp, favourite
        case userID = "user_id"
        case quotes
        case quotesID = "quotes_id"
        case favouriteDescription = "description"
        case name, image, created, modified ,source
    }
}
