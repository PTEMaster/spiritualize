//
//  File.swift
//  Spiritualize
//
//  Created by mac on 21/12/20.
//
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let login = try? newJSONDecoder().decode(Login.self, from: jsonData)

import Foundation

// MARK: - Login
struct LoginModel: Codable {
    var code, message: String?
    var userinfo: Userinfo?
}

// MARK: - Userinfo
struct Userinfo: Codable {
    var id, fullName, email, password: String?
    var profileImage, deviceType, iosToken, androidToken: String?
    var status, verify, createdAt, token: String?
    var remember,isRemind, reminder,otp: String?

    enum CodingKeys: String, CodingKey {
        case id
        case fullName = "full_name"
        case email, password
        case profileImage = "profile_image"
        case deviceType = "device_type"
        case iosToken = "ios_token"
        case androidToken = "android_token"
        case status, verify
        case createdAt = "created_at"
        case isRemind = "is_remind"
        case reminder
        case token, remember, otp
    }
}
