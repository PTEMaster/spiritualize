//
//  CollectionsVC.swift
//  Spiritualize
//
//  Created by mac on 17/12/20.
//

import UIKit
import AudioToolbox

class CollectionsVC: UIViewController {

    @IBOutlet weak var navTitleLBL: UILabel!
    @IBOutlet weak var collectionsCV: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionsCV.register(UINib(nibName: "CollectionsCell", bundle: nil), forCellWithReuseIdentifier: "CollectionsCell")
        
        self.collectionsCV.delegate = self
        self.collectionsCV.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    //  AudioServicesPlaySystemSound(1519) 
    }
     
    // MARK: - Navigation
}
extension CollectionsVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 0
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionsCell", for: indexPath) as! CollectionsCell
        cell.titleLBL.text = "Collection \(indexPath.row)"
        cell.viewWidth.constant = (self.view.bounds.width / 2) - 32
        cell.viewHeight.constant = 100
        //cell.viewHeight.constant = collectionsCV.bounds.height
        //cell.viewWidth.constant = self.view.bounds.width
   
        return cell
    }
    
}
extension CollectionsVC: UICollectionViewDelegate {
    
}
