//
//  MoreVC.swift
//  Spiritualize
//
//  Created by mac on 16/12/20.
//

import UIKit
import  SafariServices
import MessageUI
import AudioToolbox
class MoreVC: UIViewController , MFMailComposeViewControllerDelegate  {
    
    @IBOutlet weak var lblMore: UILabel!
    @IBOutlet weak var btnSwitch: UISwitch!
    @IBOutlet weak var viewRemind: UIView!
    @IBOutlet weak var btnCount: UIButton!
    @IBOutlet weak var viewRemindHeightConst: NSLayoutConstraint!
    
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var stkBtn: UIStackView!
    //
    @IBOutlet weak var btn11: UIButton!
    @IBOutlet weak var btn22: UIButton!
    @IBOutlet weak var btn33: UIButton!
    @IBOutlet weak var btn44: UIButton!
    @IBOutlet weak var btn55: UIButton!
    @IBOutlet weak var btn66: UIButton!
    @IBOutlet weak var btn77: UIButton!
    @IBOutlet weak var btn88: UIButton!
    @IBOutlet weak var btn99: UIButton!
    @IBOutlet weak var btn10: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.btnCount.setTitle(AppDataHelper.shard.logins.reminder, for: .normal)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: NSNotification.Name(rawValue: "checkPermission"), object: nil)
        
        
        
        // AudioServicesPlaySystemSound(1519) // Actuate "Peek" feedback (weak boom)
        stkBtn.isHidden = true
        // let gradientImage = UIImage.gradientImageWithBounds(bounds: lblMore.bounds, colors: [lightPurple!.cgColor, gradPurple!.cgColor])
        // lblMore.textColor = UIColor.init(patternImage: gradientImage)
        
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
        
    
        
        if #available(iOS 10.0, *) {
            let current = UNUserNotificationCenter.current()
            current.getNotificationSettings(completionHandler: { settings in
                
                switch settings.authorizationStatus {
                
                case .notDetermined:
                    print("Authorization request has not been made yet")
                    break
                case .denied:
                    DispatchQueue.main.async {
                        Util.show(message: "Please go to Settings and enable the Notifications")
                        self.viewRemindHeightConst.constant = 0
                        self.viewRemind.isHidden = true
                        self.stkBtn.isHidden = true
                        self.btnSwitch.isOn  = false
                        self.call_reminder_on_offAPI(is_remind: "0", remind: self.btnCount.titleLabel?.text ?? "1")
                    }
                    break
                case .authorized:
                    print("authorized")
                    DispatchQueue.main.async {
                    if AppDataHelper.shard.logins.isRemind == "1"{
                        self.btnSwitch.isOn = true
                        self.btnCount.setTitle(AppDataHelper.shard.logins.reminder, for: .normal)//
                        self.viewRemindHeightConst.constant = 40
                        self.viewRemind.isHidden = false
                        self.call_reminder_on_offAPI(is_remind: "1", remind: self.btnCount.titleLabel?.text ?? "1")
                       
                    }else{
                        self.viewRemindHeightConst.constant = 0
                        self.viewRemind.isHidden = true
                        self.stkBtn.isHidden = true
                        
                        self.btnSwitch.isOn = false
                        self.btnCount.setTitle(AppDataHelper.shard.logins.reminder, for: .normal)
                        
                    }
                    }
                    break
                //
                case .provisional:
                    print("provisional")
                    break
                    
                case .ephemeral:
                    print("ephemeral")
                    break
                    
                @unknown default:
                    print("default")
                    break
                    
                }
            })
            
        }
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        print("check")
        if AppDataHelper.shard.logins.isRemind == "1"{
            self.btnSwitch.isOn = true
            self.btnCount.setTitle(AppDataHelper.shard.logins.reminder, for: .normal)//
            self.viewRemindHeightConst.constant = 40
            self.viewRemind.isHidden = false
        
        }else{
            self.viewRemindHeightConst.constant = 0
            self.viewRemind.isHidden = true
            self.stkBtn.isHidden = true
            
            self.btnSwitch.isOn = false
            self.btnCount.setTitle(AppDataHelper.shard.logins.reminder, for: .normal)
        }
        
        if #available(iOS 10.0, *) {
            let current = UNUserNotificationCenter.current()
            current.getNotificationSettings(completionHandler: { settings in
                
                switch settings.authorizationStatus {
                
                case .notDetermined:
                    print("Authorization request has not been made yet")
                    break
                case .denied:
                    DispatchQueue.main.async {
                        Util.show(message: "Please go to Settings and enable the Notifications")
                        self.viewRemindHeightConst.constant = 0
                        self.viewRemind.isHidden = true
                        self.stkBtn.isHidden = true
                        self.btnSwitch.isOn  = false
                       self.call_reminder_on_offAPI(is_remind: "0", remind: self.btnCount.titleLabel?.text ?? "1")
                    }
                    break
                case .authorized:
                    print("authorized")
                   
                    DispatchQueue.main.async {
                    if AppDataHelper.shard.logins.isRemind == "1"{
                        self.btnSwitch.isOn = true
                        self.btnCount.setTitle(AppDataHelper.shard.logins.reminder, for: .normal)//
                        self.viewRemindHeightConst.constant = 40
                        self.viewRemind.isHidden = false
                        self.call_reminder_on_offAPI(is_remind: "1", remind: self.btnCount.titleLabel?.text ?? "1")
                    }else{
                        self.viewRemindHeightConst.constant = 0
                        self.viewRemind.isHidden = true
                        self.stkBtn.isHidden = true
                        
                        self.btnSwitch.isOn = false
                        self.btnCount.setTitle(AppDataHelper.shard.logins.reminder, for: .normal)
                        
                    }
                    }
                    break
                //
                case .provisional:
                    print("provisional")
                    break
                    
                case .ephemeral:
                    print("ephemeral")
                    break
                    
                @unknown default:
                    print("default")
                    break
                    
                }
            })
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("him")
    }
    @objc func hideKeyboard() {
        stkBtn.isHidden = true
    }
  
    
    @IBAction func actionSwitch(_ sender: Any) {
        if btnSwitch.isOn {
            
            if #available(iOS 10.0, *) {
                let current = UNUserNotificationCenter.current()
                current.getNotificationSettings(completionHandler: { settings in

                    switch settings.authorizationStatus {

                    case .notDetermined:
                        print("Authorization request has not been made yet")
                        break
                    case .denied:
                        // User has denied authorization.
                        // You could tell them to change this in Settings
                        print("User has denied authorization.")
                        DispatchQueue.main.async {
                                                          //  Util.show(message: "Please go to setting and enable the notification")
                            
                            self.call_reminder_on_offAPI(is_remind: "0", remind: self.btnCount.titleLabel?.text ?? "1")
                            self.viewRemindHeightConst.constant = 0
                            self.viewRemind.isHidden = true
                            self.stkBtn.isHidden = true
                        self.btnSwitch.isOn  = false
                            
                            if let bundleIdentifier = Bundle.main.bundleIdentifier, let appSettings = URL(string: UIApplication.openSettingsURLString + bundleIdentifier) {
                                if UIApplication.shared.canOpenURL(appSettings) {
                                    UIApplication.shared.open(appSettings)
                                }
                            }
                      //
                        }
                        return
                        break
                        
                    case .authorized:
                        print("authorized")
                        DispatchQueue.main.async {
                        self.viewRemindHeightConst.constant = 40
                        self.viewRemind.isHidden = false
                     //   btnCount.setTitle("1", for: .normal)
                        self.call_reminder_on_offAPI(is_remind: "1", remind: self.btnCount.titleLabel?.text ?? "1")
                        }
                        break
                        //
                    case .provisional:
                        print("provisional")
                        break
                        
                    case .ephemeral:
                        print("ephemeral")
                        break
                        
                    @unknown default:
                        print("default")
                        break
                        
                    }
                })
                
             }
        }else{
            viewRemindHeightConst.constant = 0
            viewRemind.isHidden = true
            stkBtn.isHidden = true
            call_reminder_on_offAPI(is_remind: "0", remind: self.btnCount.titleLabel?.text ?? "1")
        }
    }
    
    @IBAction func actionbtnCount(_ sender: UIButton) {
        //stackview
        stkBtn.isHidden = true
        let btntag = sender.tag
        print(btntag)
        btnCount.setTitle(String(btntag), for: .normal)
        call_reminder_on_offAPI(is_remind: "1", remind: "\(btntag)")
    }
    
    @IBAction func actionCount(_ sender: UIButton) {
        btn1.setTitleColor(gradgray, for: .normal)
        btn2.setTitleColor(gradgray, for: .normal)
        btn3.setTitleColor(gradgray, for: .normal)
        btn4.setTitleColor(gradgray, for: .normal)
        stkBtn.isHidden = false
        if btnCount.titleLabel?.text == "1"{
            btn1.setTitleColor(gradCoral, for: .normal)
        }else  if btnCount.titleLabel?.text == "2"{
            btn2.setTitleColor(gradCoral, for: .normal)
        }else if  btnCount.titleLabel?.text == "3"{
            btn3.setTitleColor(gradCoral, for: .normal)
        }else if btnCount.titleLabel?.text == "4"{
            btn4.setTitleColor(gradCoral, for: .normal)
        }else{
            btn1.setTitleColor(gradCoral, for: .normal)
        }
        
    }
    
    @IBAction func actionUrl(_ sender: UIButton) {
        let btntag = sender.tag
       print(btntag)
      if btntag == 77 || btntag == 88{
            var strUrl = ""
            if btntag == 77{
                strUrl =  "Quote Submission"
            }else{
                strUrl =  "Feedback for Spiritualize (iOS app)"
                }
            
            if MFMailComposeViewController.canSendMail() {
                let emailTitle = strUrl
                 let messageBody = ""
                 let toRecipents =  ["spiritualize.app@gmail.com"]
                 let mc: MFMailComposeViewController = MFMailComposeViewController()
                 mc.mailComposeDelegate = self
                 mc.setSubject(emailTitle)
                 mc.setMessageBody(messageBody, isHTML: false)
                 mc.setToRecipients(toRecipents)
                 self.present(mc, animated: true, completion: nil)
                } else {
                print("Application is not able to send an email")
            }
            
        }else{
            var strUrl = ""
            if btntag == 11{
                strUrl =  "https://www.instagram.com/spiritualize.app/"
           }else if btntag == 22{
            strUrl =  "fb://profile/105163564720292"
           }
           else if btntag == 33{
            strUrl = "https://www.youtube.com/channel/UCr0kIQadB4lXqJG9YBC8RQg/"
               
           }
           else if btntag == 44{
            strUrl = "https://www.tiktok.com/@spiritualize.app/"
           }
           else if btntag == 55{
            strUrl =  "https://twitter.com/spiritualizeapp/"
           }
           else if btntag == 66{
            strUrl = "https://www.pinterest.com/spiritualize/"
           }
           else if btntag == 99{
            strUrl = "\(baseUrl)getPrivacypolicy/"
           }
           else if btntag == 10{
            strUrl = "\(baseUrl)getTermsncondition/"
           }
            
            
            let instagramHooks = strUrl
            let instagramUrl = NSURL(string: instagramHooks)
                 if UIApplication.shared.canOpenURL(instagramUrl! as URL)
                     {
                  //    UIApplication.shared.openURL(instagramUrl! as URL)
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(instagramUrl! as URL, options: [:], completionHandler: nil)
                            } else {
                                UIApplication.shared.openURL(instagramUrl! as URL)
                            }

                      } else {
                        if btntag == 22{
                            strUrl = "https://www.facebook.com/spiritualizeapp/"
                        }
                        let vc = SFSafariViewController(url: URL(string: strUrl)!)
                        vc.delegate = self
                        self.present(vc, animated: true, completion: nil)
                     }
        }
        
/*
         if btntag == 11{
            let vc = SFSafariViewController(url: URL(string: "https://www.instagram.com/spiritualize.app/")!)
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }else if btntag == 22{
            let vc = SFSafariViewController(url: URL(string: "https://www.facebook.com/spiritualizeapp/")!)
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
        else if btntag == 33{
            let vc = SFSafariViewController(url: URL(string: "https://www.youtube.com/channel/UCr0kIQadB4lXqJG9YBC8RQg/")!)
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
        else if btntag == 44{
            let vc = SFSafariViewController(url: URL(string: "https://www.tiktok.com/@spiritualize.app/")!)
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
        else if btntag == 55{
            let vc = SFSafariViewController(url: URL(string: "https://twitter.com/spiritualizeapp/")!)
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
        else if btntag == 66{
            let vc = SFSafariViewController(url: URL(string: "https://www.pinterest.com/spiritualize/")!)
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
        
         else if btntag == 77{
            if MFMailComposeViewController.canSendMail() {
            let emailTitle = "Quote Submission"
             let messageBody = ""
             let toRecipents = ["spiritualize.app@gmail.com"]
             let mc: MFMailComposeViewController = MFMailComposeViewController()
             mc.mailComposeDelegate = self
             mc.setSubject(emailTitle)
             mc.setMessageBody(messageBody, isHTML: false)
             mc.setToRecipients(toRecipents)
             self.present(mc, animated: true, completion: nil)
            } else {
                print("Application is not able to send an email")
            }
        }else if btntag == 88{
            if MFMailComposeViewController.canSendMail() {
                let emailTitle = "Feedback for Spiritualize (iOS app)"
                 let messageBody = ""
                 let toRecipents = ["spiritualize.app@gmail.com"]
                 let mc: MFMailComposeViewController = MFMailComposeViewController()
                 mc.mailComposeDelegate = self
                 mc.setSubject(emailTitle)
                 mc.setMessageBody(messageBody, isHTML: false)
                 mc.setToRecipients(toRecipents)
                 self.present(mc, animated: true, completion: nil)
                } else {
                print("Application is not able to send an email")
            }
        }*/
    }
    
    @IBAction func sendEmail(_ sender: UIButton) {
       let emailTitle = "Feedback"
        let messageBody = ""
        let toRecipents = ["admin@masterpte.com.au"]
        let mc: MFMailComposeViewController = MFMailComposeViewController()
        mc.mailComposeDelegate = self
        mc.setSubject(emailTitle)
        mc.setMessageBody(messageBody, isHTML: false)
        mc.setToRecipients(toRecipents)
        self.present(mc, animated: true, completion: nil)
        
         /*      //TODO:  You should chack if we can send email or not
               if MFMailComposeViewController.canSendMail() {
                   let mail = MFMailComposeViewController()
                   mail.mailComposeDelegate = self
                   mail.setToRecipients(["you@yoursite.com"])
                   mail.setSubject("Email Subject Here")
                   mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
                   present(mail, animated: true)
               } else {
                   print("Application is not able to send an email")
               }
        */
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
}


extension MoreVC: SFSafariViewControllerDelegate {
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        print("safari did finish")
    }
    
    func safariViewController(_ controller: SFSafariViewController, initialLoadDidRedirectTo URL: URL) {
        print("url\(URL.absoluteString)")
        let urlString = "https://www.google.co.in/"
        
        if URL.absoluteString == urlString {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                // do stuff 42 seconds later
                self.dismiss(animated: true) {
                    NotificationCenter.default.post(name: Notification.Name("popAndPush"), object: nil)
                    self.navigationController?.popViewController(animated: true)
                    
                }
            }
        }
    }
}

extension MoreVC{
    func call_reminder_on_offAPI(is_remind : String , remind : String ){
        var parameter = [String : Any]()
        parameter[params.kuser_id] = AppDataHelper.shard.logins.id
        parameter[params.kis_remind] = is_remind
        parameter[params.kremind] = remind
        
        ServerManager.shared.POST(url: ApiAction.reminder_on_off  , param: parameter, header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            
            guard  let obj = try? JSONDecoder().decode(LoginModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                UserDefaults.standard.setLoggedIn(value: true)
                AppDataHelper.shard.logins = obj.userinfo
                UserDefaults.standard.logoutData()
                UserDefaults.standard.setUserData(value: data)
                
            } else {
                presentAlert(kAppName, msgStr: obj.message, controller: self)
            }
        }
    }
}



 



    

