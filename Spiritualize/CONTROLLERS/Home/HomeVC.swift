//
//  HomeVC.swift
//  Spiritualize
//
//  Created by mac on 14/12/20.
//

import UIKit
import AudioToolbox

class HomeVC: KBaseViewController,FavUnFavDelegate, UNUserNotificationCenterDelegate {
  
    
    private let refreshControl = UIRefreshControl()

    
    //Outlets
    @IBOutlet weak var lblSpiritual: UILabel!
    @IBOutlet weak var cvSpiritual: UICollectionView!
    @IBOutlet weak var btnHeart: UIButton!
    @IBOutlet weak var viewGuide: UIView!
    lazy var arrquotes = [Quote]()
    
    
    //LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
       /* refreshControl.addTarget(self, action: #selector(didPullToRefresh(_:)), for: .valueChanged)
        cvSpiritual.alwaysBounceVertical = true
        cvSpiritual.refreshControl = refreshControl*/
       
        viewGuide.isHidden = true
//        let gradientImage = UIImage.gradientImageWithBounds(bounds: lblSpiritual.bounds, colors: [lightPurple!.cgColor, gradPurple!.cgColor])
//        lblSpiritual.textColor = UIColor.init(patternImage: gradientImage)
        btnHeart.isHidden = true
        self.cvSpiritual.register(UINib(nibName: "SpiritualizeCell", bundle: nil), forCellWithReuseIdentifier: "SpiritualizeCell")
        self.cvSpiritual.delegate = self
        self.cvSpiritual.dataSource = self
        
        self.tabBarController!.tabBar.layer.borderWidth = 0.50
        self.tabBarController!.tabBar.layer.borderColor = seperator?.cgColor
        self.tabBarController?.tabBar.clipsToBounds = true
        
        }
    
 /*   @objc
    private func didPullToRefresh(_ sender: Any) {
        // Do you your api calls in here, and then asynchronously remember to stop the
        // refreshing when you've got a result (either positive or negative)
        
        
        call_GetQuotesAPI(appDelegate.myQouteId)
        
        
    }*/
    
        @objc func methodOfReceivedNotification(notification: Notification) {
           
        let notificationType = notification.object as? [String:Any]
            
          print("notificationType=-\(notificationType!)-")
            if  let notiType =  notificationType?["gcm.notification.type"] as? String{
                if notiType == "New Quotes"{
                    if   let str = notificationType?["gcm.notification.text"] as? String{
                   
               if let data = str.data(using: String.Encoding.utf8) {
                            do {
                            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                UserDefaults.standard.set(json, forKey: "aa")
                               
                                let quotes_id = json["quotes_id"] as? String
                               // Util.show(message: "\(quotes_id)")
                                appDelegate.myQouteId =  quotes_id ?? "1" //"\(quotes_id!)"
                        //   call_GetQuotesAPI(String(quotes_id!))
                                call_GetQuotesAPI(quotes_id ?? "1")
                            } catch {
                            print("Something went wrong")
                            }
                            }
                }
                }
            }
        }
    


    
    func isUpdateAvailable() throws -> (Bool , String) {
        guard let info = Bundle.main.infoDictionary,
            let currentVersion = info["CFBundleShortVersionString"] as? String,
            let identifier = info["CFBundleIdentifier"] as? String,
            let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)") else {
            throw VersionError.invalidBundleInfo
        }
        let data = try Data(contentsOf: url)
        guard let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any] else {
            throw VersionError.invalidResponse
        }
        if let result = (json["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String {
          //  print(result)
           // print( result["version"] as? String)
            
           let bb = version != currentVersion
            return (bb ,result["version"] as! String )
        }
        throw VersionError.invalidResponse
    }
   
    
    
    override func viewWillAppear(_ animated: Bool) {
        arrquotes.removeAll()
        cvSpiritual.reloadData()
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: NSNotification.Name(rawValue: "receive_notification_identifier"), object: nil)
        
        if appDelegate.isCallViewWillAppear{
          //  AudioServicesPlaySystemSound(1519)
          //  Util.show(message: "\(appDelegate.isCallViewWillAppear)")
            call_GetQuotesAPI(appDelegate.myQouteId)
        }
        
    //   call_GetQuotesAPI(appDelegate.myQouteId )
       if (UserDefaults.standard.value(forKey: "isShowIntroScreen") as? String) == nil{
        UserDefaults.standard.set("1", forKey: "isShowIntroScreen")
        viewGuide.isHidden = false
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
                let blurEffectView = UIVisualEffectView(effect: blurEffect)
                blurEffectView.frame = viewGuide.bounds
        blurEffectView.backgroundColor = UIColor(red: 41/255, green: 41/255, blue: 41/255, alpha:1)
              blurEffectView.alpha = 0.8
                blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewGuide.insertSubview(blurEffectView, at: 0)
        self.test(viewTest: viewGuide)
       }else{
        self.registerForRemoteNotification()
        DispatchQueue.global().async {
            do {
                let update = try self.isUpdateAvailable()
                DispatchQueue.main.async {
                    print(update.0)
                    print(update.1)
               
                  
                    if update.0{
                        DispatchQueue.main.async(execute: {
                            if (UserDefaults.standard.value(forKey: "version") as? String) != nil{
                           let version = UserDefaults.standard.value(forKey: "version")as? String
                                if version  == update.1{
                                    return
                                }
                                
                            }
                            let _ = presentAlertWithOptions("New Version Available", message: "A new version of Spiritualize Application is available,Please update to version " + update.1, controller: self, buttons: ["Ok" , "Cancel"]) { (alert, actionTag) in
                                if actionTag == 0 {
                                    //Ok Button
                                   
                                    if let url = URL(string:"itms-apps://apple.com/app/id1548640792") {
                                        UIApplication.shared.open(url)
                                    }
                                   
                                } else {
                                    //Cancel Button
                                    UserDefaults.standard.set(update.1, forKey: "version")
                                    print("Cancel Button")
                                }
                            }
                        })
                     
                       /* Util.showAlertWithCallback("New Version Available", message: "A new version of Spiritualize Application is available,Please update to version " + update.1, isWithCancel: true){
                            if let url = URL(string:"itms-apps://apple.com/app/id1548640792") {
                                UIApplication.shared.open(url)
                            }
                        }*/
                    }
                }
            } catch {
                print(error)
            }
        }
        
        
       }
    }
    

    
    override func viewWillDisappear(_ animated: Bool) {
        appDelegate.isCallViewWillAppear = true
        NotificationCenter.default.removeObserver(self)
    }
    
    
    func test(viewTest: UIView) {
        let orignalT: CGAffineTransform = viewTest.transform
        viewTest.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
        UIView.animate(withDuration: 0.4, animations: {
            viewTest.transform = orignalT
        }, completion:{_ in
            let seconds = 7.0
            DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
                // Put your code which should be executed with a delay here
                self.fadeOut()
            }
        })
    }
    
   
    func registerForRemoteNotification() {
        UNUserNotificationCenter.current().delegate = self
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()

            center.requestAuthorization(options: [.sound, .alert, ]) { (granted, error) in
                if error == nil{
                    DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
                }
            }

        }
        else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, ], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    //----------
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, openSettingsFor notification: UNNotification?) {
       print("notihpp1111111111")
        UserDefaults.standard.set("1", forKey: "hp")
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("notihpp222222234343")
        let userInfo = response.notification.request.content.userInfo
       // NotificationCenter.default.post(name: Notification.Name("receive_notification_identifier"), object: userInfo, userInfo: userInfo)
        self.tabBarController?.selectedIndex = 0
        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "receive_notification_identifier"), object: userInfo, userInfo: userInfo))
        
        completionHandler()
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("notihpp333333333333333")
       // print(notification.request.content.body);
      let userInfo = notification.request.content.userInfo
              print("notifiction \(userInfo))")
       
     // NotificationCenter.default.post(name: Notification.Name("receive_notification_identifier"), object: userInfo)
        completionHandler([.alert, .sound])
    }
    


}

extension HomeVC: UICollectionViewDataSource , UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrquotes.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SpiritualizeCell", for: indexPath) as! SpiritualizeCell
        cell.viewHeight.constant = cvSpiritual.frame.height
        cell.viewWidth.constant = cvSpiritual.frame.width
        cell.likeBTN.tag = indexPath.row
        let dict =  arrquotes[indexPath.row]
        cell.descriptionLBL.textColor = gradPurple
        if dict.fontColor == "Purple"{
            cell.descriptionLBL.textColor = gradPurple
        }else if dict.fontColor == "Coral"{
            cell.descriptionLBL.textColor = gradCoral
        }else{
            cell.descriptionLBL.textColor = gradPurple
        }
        cell.descriptionLBL.text = dict.quoteDescription?.decodeEmoji
       // let gradientImage = UIImage.gradientImageWithBounds(bounds: cell.descriptionLBL.bounds, colors: [lightPurple!.cgColor, gradPurple!.cgColor])
       // cell.descriptionLBL.textColor = UIColor.init(patternImage: gradientImage)
        cell.nameLBL.text = dict.name?.decodeEmoji
      //  cell.addressLBL.text = dict.email
        cell.delegate = self
        if dict.favourite  == "1"{
            cell.likeBTN.isSelected = true
        }else{
            cell.likeBTN.isSelected = false
        }
        cell.likeBTN.tag = indexPath.row
        cell.likeBTN.addTarget(self, action: #selector(likeButtonCLick), for: .touchUpInside)
        
       cell.btnDoubleTab.tag = indexPath.row
        cell.lblNameCenter.isHidden = true
        cell.nameLBL.isHidden = true
        cell.addressLBL.isHidden = true
        if dict.name != "" && dict.source != ""{
            cell.nameLBL.isHidden = false
            cell.addressLBL.isHidden = false
            cell.nameLBL.text = dict.name
            cell.addressLBL.text = dict.source?.decodeEmoji
        }else if dict.name != "" && dict.source == ""{
            cell.lblNameCenter.isHidden = false
            cell.lblNameCenter.text = dict.name
        }else{
            cell.lblNameCenter.isHidden = false
            cell.lblNameCenter.text = dict.source
        }
        
        return cell
    }
 
   /* func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let visibleIndexPath = collectionView.indexPathForItem(at: visiblePoint)
        print("visibleIndexPath\(visibleIndexPath)")
    }*/
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        visibleRect.origin = cvSpiritual.contentOffset
        visibleRect.size = cvSpiritual.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
       guard let indexPath = cvSpiritual.indexPathForItem(at: visiblePoint) else { return }
        //print(indexPath.row)
       let id = arrquotes[indexPath.row].quotesID
        appDelegate.myQouteId  = id!
    // call_swipeQuotesAPI(quotesId: id ?? "0")
        call_swipeQuotesAPI(quotesId: id ?? "0", currentIndex: indexPath.row)
        
    }
    

    
    
    
    
    func doubleTab(cell: SpiritualizeCell) {
        guard let indexPath = cvSpiritual.indexPath(for: cell) else {
             return
         }
        let dict = arrquotes[indexPath.row]
        if cell.likeBTN.isSelected {
            cell.likeBTN.isSelected = false
            print("selected")
            
            call_addLikeAPI(isFav: "0", indexPath: indexPath, quotesId: dict.quotesID ?? "0")
        }else {
            cell.likeBTN.isSelected = true
            btnHeart.isHidden = false
            btnHeart.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            btnHeart.setImage(UIImage(named: "Heart"), for: .normal)
            UIView.animate(withDuration: 1.0,
                           delay: 0,
                           usingSpringWithDamping: 0.3,
                           initialSpringVelocity: 8.0,
                           options: .allowUserInteraction,
                           animations: {
                            self.btnHeart.transform = .identity
                           },
                           completion:  { [self] finished in
                            btnHeart.isHidden = true
                            btnHeart.setImage(UIImage(named: ""), for: .normal)
                           })
            call_addLikeAPI(isFav: "1",  indexPath: indexPath, quotesId: dict.quotesID ?? "0")
        }
        
    }
    
    
    func MakeFavUnfav(cell: SpiritualizeCell) {
        guard let indexPath = cvSpiritual.indexPath(for: cell) else {
             return
         }
        let dict = arrquotes[indexPath.row]
        if dict.favourite == "1"{
            call_addLikeAPI(isFav: "0", indexPath: indexPath, quotesId: dict.quotesID ?? "0")
             
        }else{
            call_addLikeAPI(isFav: "1",  indexPath: indexPath, quotesId: dict.quotesID ?? "0")
        }
    }
    
    
  
    
    @objc func likeButtonCLick(_ sender: UIButton){
        //btnLikeUnlike.isSelected = true
        if sender.isSelected {
            sender.isSelected = false
            print("selected")
        }else {
            sender.isSelected = true
            btnHeart.isHidden = false
            btnHeart.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            btnHeart.setImage(UIImage(named: "Heart"), for: .normal)
            UIView.animate(withDuration: 1.0,
                           delay: 0,
                           usingSpringWithDamping: 0.3,
                           initialSpringVelocity: 8.0,
                           options: .allowUserInteraction,
                           animations: {
                            self.btnHeart.transform = .identity
                           },
                           completion:  { [self] finished in
                            btnHeart.isHidden = true
                            btnHeart.setImage(UIImage(named: ""), for: .normal)
                           })
        }
    }
    
    
    
}

extension HomeVC {
    func call_GetQuotesAPI(_ quotesId : String = ""){
        var parameter = [String : Any]()
        let date =   Date().string(format: "yyyy-MM-dd HH:mm:ss")
        parameter[params.kuser_id] = AppDataHelper.shard.logins.id
        parameter[params.klast_date] = date
        parameter[params.kquotes_id] = quotesId
            // Mark:- fixed
        parameter[params.kcur_timezone] =  TimeZone.current.identifier
        
        ServerManager.shared.POST(url: ApiAction.getQuotes  , param: parameter, header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(GetQuoteModal.self, from: data) else {
                return
            }
          
            if obj.code == ResponseApis.KSuccess {
               // self.refreshControl.endRefreshing()
                self.arrquotes.removeAll()
                self.arrquotes = obj.quotes!
                self.cvSpiritual.reloadData()
                if self.arrquotes.count > 0{
                  let id = self.arrquotes.first?.quotesID
                    appDelegate.myQouteId  = id!
                    print(self.arrquotes.count)
                   self.call_swipeQuotesAPI(quotesId: id ?? "0")
                    }
            } else {
               // self.refreshControl.endRefreshing()
                self.arrquotes.removeAll()
                self.cvSpiritual.reloadData()
              //  presentAlert(kAppName, msgStr: obj.message, controller: self)
            }
        }
    }
    
    func call_addLikeAPI(isFav : String,  indexPath : IndexPath ,quotesId : String ){
        var parameter = [String : Any]()
        parameter[params.kuser_id] = AppDataHelper.shard.logins.id
        parameter[params.kfavourite] = isFav
        parameter[params.kquotes_id] = quotesId
        
       
        ServerManager.shared.POST(url: ApiAction.addLike  , param: parameter, false,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(GetQuoteModal.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
              self.arrquotes[indexPath.row].favourite = isFav
            } else {
                
              //  presentAlert(kAppName, msgStr: obj.message, controller: self)
            }
        }
    }
    
    func fadeOut( duration: TimeInterval = 1.0) {
        UIView.animate(withDuration: duration, animations: {
            self.viewGuide.alpha = 0.0
            self.registerForRemoteNotification()
    })
  }
    
    @IBAction func closeGuidePopup(_ sender: UIButton) {
        fadeOut()
    }
    
            // Mark:- newapi
    func call_swipeQuotesAPI(quotesId : String , currentIndex : Int? = 0 ){
        var parameter = [String : Any]()
            let date =   Date().string(format: "yyyy-MM-dd HH:mm:ss")
        
        parameter[params.kuser_id] = AppDataHelper.shard.logins.id
        parameter[params.klast_date] = date
        parameter[params.kquotes_id] = quotesId
        parameter[params.kis_read] = "1"
        
       
        ServerManager.shared.POST(url: ApiAction.swipeQuotes  , param: parameter, false,header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
//            guard  let obj = try? JSONDecoder().decode(GetQuoteModal.self, from: data) else {
//                return
//            }
            if (self.arrquotes.count - 1) == currentIndex{
                self.call_GetQuotesAPI("")
            }
           
        }
    }
}



extension UICollectionView {
  var visibleCurrentCellIndexPath: IndexPath? {
    for cell in self.visibleCells {
      let indexPath = self.indexPath(for: cell)
      return indexPath
    }
    
    return nil
  }
}


extension Date {
    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}




extension String {
    var decodeEmoji: String{
        let data = self.data(using: String.Encoding.utf8);
        let decodedStr = NSString(data: data!, encoding: String.Encoding.nonLossyASCII.rawValue)
        if let str = decodedStr{
            return str as String
        }
        return self
    }
    var encodeEmoji: String{
        if let encodeStr = NSString(cString: self.cString(using: .nonLossyASCII)!, encoding: String.Encoding.utf8.rawValue){
            return encodeStr as String
        }
        return self
    }
}


enum VersionError: Error {
    case invalidResponse, invalidBundleInfo
}
