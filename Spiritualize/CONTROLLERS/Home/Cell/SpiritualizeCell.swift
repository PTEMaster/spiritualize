//
//  SpiritualizeCell.swift
//  Spiritualize
//
//  Created by mac on 17/12/20.
//

import UIKit
protocol FavUnFavDelegate:NSObject {
    func MakeFavUnfav(cell:SpiritualizeCell)
    func doubleTab(cell:SpiritualizeCell)
    
    
}


class SpiritualizeCell: UICollectionViewCell {

    @IBOutlet weak var likeBTN: UIButton!
    @IBOutlet weak var btnDoubleTab: UIButton!
    @IBOutlet weak var nameLBL: UILabel!
    @IBOutlet weak var lblNameCenter: UILabel!
    @IBOutlet weak var addressLBL: UILabel!
    @IBOutlet weak var scrlView: UIScrollView!
    
    
    
    @IBOutlet weak var descriptionLBL: UILabel!
    
    @IBOutlet weak var viewWidth: NSLayoutConstraint!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    weak var delegate: FavUnFavDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
            tap.numberOfTapsRequired = 2
        scrlView.addGestureRecognizer(tap)
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
            tap2.numberOfTapsRequired = 2
       btnDoubleTab.addGestureRecognizer(tap2)
    }
    
 
    @IBAction func likeBTN_Action(_ sender: Any) {
        delegate?.MakeFavUnfav(cell: self)
    }
    
    
    @IBAction func doubleTab_Action(_ sender: Any) {
        
    }
    @objc func doubleTapped() {
        delegate?.doubleTab(cell: self)
        
    }
}
