//
//  FavoriteVC.swift
//  Spiritualize
//
//  Created by mac on 17/12/20.
//

import UIKit
import AudioToolbox


class FavoriteVC: UIViewController, FavUnFavDelegate {
    
    
    //Outlets
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var lblFavorite: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var cvFavorite: UICollectionView!
    @IBOutlet weak var btnHeart: UIButton!
    
    var arrquotes = [Favourite]()
    var finalCount = 0
    var currentCount = 0
    //LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  let gradientImage = UIImage.gradientImageWithBounds(bounds: lblFavorite.bounds, colors: [lightPurple!.cgColor, gradPurple!.cgColor])
      //  lblFavorite.textColor = UIColor.init(patternImage: gradientImage)
        btnHeart.isHidden = true
        lblMsg.isHidden = true
        
        self.cvFavorite.register(UINib(nibName: "SpiritualizeCell", bundle: nil), forCellWithReuseIdentifier: "SpiritualizeCell")
        self.cvFavorite.delegate = self
        self.cvFavorite.dataSource = self
        
       
    }
    override func viewWillAppear(_ animated: Bool) {
      // AudioServicesPlaySystemSound(1519) 
        call_getFavouritesAPI()
    }
    
    //All Action
    @IBAction func actionLikeUnlike(_ sender: Any) {
        
    }
}

extension FavoriteVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrquotes.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SpiritualizeCell", for: indexPath) as! SpiritualizeCell
        cell.viewHeight.constant = cvFavorite.frame.height
        cell.viewWidth.constant = cvFavorite.frame.width
        cell.likeBTN.tag = indexPath.row
        let dict =  arrquotes[indexPath.row]
        cell.descriptionLBL.text = dict.favouriteDescription?.decodeEmoji
        cell.nameLBL.text = dict.name?.decodeEmoji
      //  cell.addressLBL.text = dict.email
        cell.delegate = self
        if dict.favourite  == "1"{
            cell.likeBTN.isSelected = true
        }else{
            cell.likeBTN.isSelected = false
        }
            
        cell.likeBTN.addTarget(self, action: #selector(likeButtonCLick), for: .touchUpInside)
        
        
        if dict.fontColor == "Purple"{
            cell.descriptionLBL.textColor = gradPurple
        }else if dict.fontColor == "Coral"{
            cell.descriptionLBL.textColor = gradCoral
        }else{
            cell.descriptionLBL.textColor = gradPurple
        }
        
        
        cell.btnDoubleTab.tag = indexPath.row
         cell.lblNameCenter.isHidden = true
         cell.nameLBL.isHidden = true
         cell.addressLBL.isHidden = true
         if dict.name != "" && dict.source != ""{
             cell.nameLBL.isHidden = false
             cell.addressLBL.isHidden = false
             cell.nameLBL.text = dict.name
             cell.addressLBL.text = dict.source?.decodeEmoji
         }else if dict.name != "" && dict.source == ""{
             cell.lblNameCenter.isHidden = false
             cell.lblNameCenter.text = dict.name
         }else{
             cell.lblNameCenter.isHidden = false
             cell.lblNameCenter.text = dict.source
         }
        
        
        
        
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        visibleRect.origin = cvFavorite.contentOffset
        visibleRect.size = cvFavorite.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let indexPath = cvFavorite.indexPathForItem(at: visiblePoint) else { return }
        currentCount = indexPath.row + 1
        lblCount.text = "\(currentCount) / \(finalCount)"
        appDelegate.myFavQouteId = indexPath.row
        print(appDelegate.myFavQouteId)
        print(arrquotes.count - 1 )
        
    }
    
    func doubleTab(cell: SpiritualizeCell) {
        guard let indexPath = cvFavorite.indexPath(for: cell) else {
             return
         }
        let dict = arrquotes[indexPath.row]
        if cell.likeBTN.isSelected {
            cell.likeBTN.isSelected = false
            print("selected")
            call_addLikeAPI(isFav: "0", indexPath: indexPath, quotesId: dict.quotesID ?? "0")
        }else {
            call_addLikeAPI(isFav: "1", indexPath: indexPath, quotesId: dict.quotesID ?? "0")
            cell.likeBTN.isSelected = true
            btnHeart.isHidden = false
            btnHeart.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            btnHeart.setImage(UIImage(named: "Heart"), for: .normal)
            UIView.animate(withDuration: 1.0,
                           delay: 0,
                           usingSpringWithDamping: 0.3,
                           initialSpringVelocity: 8.0,
                           options: .allowUserInteraction,
                           animations: {
                            self.btnHeart.transform = .identity
                           },
                           completion:  { [self] finished in
                            btnHeart.isHidden = true
                            btnHeart.setImage(UIImage(named: ""), for: .normal)
                           })
        }
    }
    
    func MakeFavUnfav(cell: SpiritualizeCell) {
        guard let indexPath = cvFavorite.indexPath(for: cell) else {
             return
         }
        let dict = arrquotes[indexPath.row]
        if dict.favourite == "1"{
            call_addLikeAPI(isFav: "0", indexPath: indexPath, quotesId: dict.quotesID ?? "0")
             
        }else{
            call_addLikeAPI(isFav: "1", indexPath: indexPath, quotesId: dict.quotesID ?? "0")
        }
    }
    
    @objc func likeButtonCLick(_ sender: UIButton){
        //btnLikeUnlike.isSelected = true
        if sender.isSelected {
            sender.isSelected = false
            print("selected")
        }else {
            sender.isSelected = true
            btnHeart.isHidden = false
            btnHeart.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            btnHeart.setImage(UIImage(named: "Heart"), for: .normal)
            UIView.animate(withDuration: 1.0,
                           delay: 0,
                           usingSpringWithDamping: 0.3,
                           initialSpringVelocity: 8.0,
                           options: .allowUserInteraction,
                           animations: {
                            self.btnHeart.transform = .identity
                           },
                           completion:  { [self] finished in
                            btnHeart.isHidden = true
                            btnHeart.setImage(UIImage(named: ""), for: .normal)
                           })
        }
       // cvFavorite.reloadData()
        
    }
    
}
extension FavoriteVC: UICollectionViewDelegate {
    func call_getFavouritesAPI(){
        var parameter = [String : Any]()
        parameter[params.kuser_id] = AppDataHelper.shard.logins.id
        ServerManager.shared.POST(url: ApiAction.getFavourites  , param: parameter, header: nil) { [self] (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(FavouriteModel.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self.arrquotes.removeAll()
                self.arrquotes = obj.favourite!
                self.cvFavorite.reloadData()
                self.finalCount = self.arrquotes.count
                self.currentCount = 1
                self.lblCount.text = "1" + " / " + "\(self.finalCount)"
                self.lblMsg.isHidden = true
                if (arrquotes.count - 1 ) >= appDelegate.myFavQouteId{
                    cvFavorite.setNeedsLayout()
                    cvFavorite.layoutIfNeeded()
                   
                    lblCount.text = "\(appDelegate.myFavQouteId + 1) / \(finalCount)"
                    cvFavorite.scrollToItem(at: NSIndexPath(item: appDelegate.myFavQouteId, section: 0) as IndexPath,at: .top,animated: false)
                }
            } else {
                self.arrquotes.removeAll()
                self.cvFavorite.reloadData()
                self.lblCount.text = "0 / 0"
                self.lblMsg.isHidden = false
               // presentAlert(kAppName, msgStr: obj.message, controller: self)
            }
        }
    }
    
    func call_addLikeAPI(isFav : String, indexPath : IndexPath ,quotesId : String ){
        var parameter = [String : Any]()
        parameter[params.kuser_id] = AppDataHelper.shard.logins.id
        parameter[params.kquotes_id] = quotesId
        parameter[params.kfavourite] = isFav
        
        ServerManager.shared.POST(url: ApiAction.addLike  , param: parameter, header: nil) { (data, error) in
            guard let data = data else {
                print("data not available")
                return
            }
            guard  let obj = try? JSONDecoder().decode(GetQuoteModal.self, from: data) else {
                return
            }
            if obj.code == ResponseApis.KSuccess {
                self.arrquotes[indexPath.row].favourite = isFav
                //self.arrquotes.remove(at: indexPath.row)
                //self.cvFavorite.deleteItems(at: [indexPath])
               // self.cvFavorite.reloadData()
            } else {
                presentAlert(kAppName, msgStr: obj.message, controller: self)
            }
        }
    }
}
