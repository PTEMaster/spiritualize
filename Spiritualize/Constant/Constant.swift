//
//  Constent.swift
//  Auction
//
//  Created by mac on 10/12/20.
//  https://www.spiritualize.app/

import Foundation
import  UIKit
let appDelegate = UIApplication.shared.delegate as! AppDelegate

let kAppName = "Spiritualize"
let kisLogin = "isLogin"
let baseUrl  =  "https://www.spiritualize.app/api/"
let ImageUrl =  "https://www.spiritualize.app/assets/profile_image/"

//let baseUrl  =  "https://ctinfotech.com/CTCC/spiritualize/api/"
//let ImageUrl =  "https://ctinfotech.com/CTCC/spiritualize/assets/profile_image/"



let gradPurple = UIColor(named: "purple")
let  lightPurple = UIColor(named: "lightPurple")
let gradWhite = UIColor(named: "white")
let gradgray = UIColor(named: "grey")
let gradCoral = UIColor(named: "Coral")
let seperator = UIColor(named: "seperator")


//struct ImageUrl {
//    static let ImageUrl = "https://ctinfotech.com/CTCC/spiritualize/assets/profile_image/"
//}




struct ApiAction {
    static let signup = "signup"
    static let getQuotes = "getQuotes"
    static let addLike = "addLike"
    static let getFavourites = "getFavourites"
    static let reminder_on_off = "reminder_on_off"
    static let swipeQuotes = "swipeQuotes"
}



//MARK:- Validation string
enum Validation: String {
    case kEnterName = "Please Enter Your Name"
    case kEnterPassword = "Please Enter Your Password"
    case kEnterEmail = "Please Enter Your Email Address"
    case kEnterValidEmail = "Please enter valid Email Address"
    case kEnterConfirmPassword = "Please Enter Confirm Password"
    case kEnterNewPassword = "Please Enter New Password"
    case kEnterValidPassword = "Please Enter Valid Password"
}

//MARK:- Button titile string
enum ButtonTitle: String {
    case kOk = "OK"
    case kCancel = "CANCEL"
    case kYes = "YES"
    case kNo = "NO"
}


struct Message {
    static let msgSorry            = "Sorry something went wrong."
    static let msgTimeOut          = "Request timed out."
    static let msgCheckConnection  = "Please check your connection and try again."
    static let msgConnectionLost   = "Network connection lost."
    static let Key_Alert           = ""
    static let couldNotConnect     = "Could not able to connect with server. Please try again."
    static  let networkAlertMessage   = "No Internet Connection"
    
    static let newVersion = ""
}

struct params {
    static let kfull_name  = "full_name"
    static let kemail = "email"
    static let kios_token = "ios_token"
    static let kdevice_type = "device_type"
    static let kis_email_permission = "is_email_permission"
    //
    static let kuser_id  = "user_id"
   // static let kfav_id  = "fav_id"
    static let kfavourite  = "favourite"
    static let kquotes_id  = "quotes_id"
    static let kis_read  = "is_read"
    static let klast_date  = "last_date"
    static let kis_remind  = "is_remind"
    static let kremind  = "remind"
    static let kcur_timezone  = "cur_timezone"
    
    
    
}



class APPConstants: NSObject {
    static let emailAcceptableCharacter = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_.-@"
    static let AddressAcceptableCharacter = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890,/- "
    static let NameAcceptableCharacter = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "
    static let ZipCodeAcceptableCharacter = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
    static let StateCharacter = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 ."
    static let phoneNoAcceptableCharacter = "1234567890"
}
