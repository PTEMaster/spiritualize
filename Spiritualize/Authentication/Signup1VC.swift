//
//  Signup1VC.swift
//  Spiritualize
//
//  Created by mac on 11/12/20.
//

import UIKit
import UserNotifications

class Signup1VC: KBaseViewController , UITextFieldDelegate{
     
    @IBOutlet weak var btnContinue: GradientButton!
    @IBOutlet weak var tfName: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnContinue.isSelected = false
        btnContinue.isEnabled = false
        tfName.setLeftPaddingPoints(10)
        tfName.addTarget(self, action: #selector(SearchDidChange(_:)), for: .editingChanged)
     
    }

    
    
    @objc func SearchDidChange(_ textField: UITextField) {
        if textField.text?.count ?? 0 > 1{
            btnContinue.isSelected = true
            btnContinue.isEnabled = true
        }else{
            btnContinue.isSelected = false
            btnContinue.isEnabled = false
        }
    }
   

    @IBAction func actionContinue(_ sender: Any) {
    
        let vc = Signup2VC.instance(storyBoard: .main) as! Signup2VC
        vc.userName = tfName.text ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
         let rawString = string
         let range1 = rawString.rangeOfCharacter(from: .whitespaces)
         if ((textField.text?.count)! == 0 && range1  != nil)
             || ((textField.text?.count)! > 0  && range1 != nil && textField != tfName)  {
             return false
         }
         let length = (textField.text?.count)! + string.count - range.length
         
      if textField == tfName {
                   let validCharacterSet = NSCharacterSet(charactersIn: APPConstants.NameAcceptableCharacter).inverted
                   let filter = string.components(separatedBy: validCharacterSet)
                   if filter.count == 1 {
                       
                       return (length > 100) ? false : true
                   } else {
                       return false
                   }
               }
                   
             
               return true
         
         
     }

}


