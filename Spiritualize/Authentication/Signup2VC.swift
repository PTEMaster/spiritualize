//
//  Signup2VC.swift
//  Spiritualize
//
//  Created by mac on 14/12/20.
//

import UIKit

class Signup2VC: KBaseViewController , UITextFieldDelegate{
    
    @IBOutlet weak var btnLetsGo: GradientButton!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMotivational : UILabel!
    
    
    var userName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnLetsGo.isSelected = false
        btnLetsGo.isEnabled = false
        tfEmail.setLeftPaddingPoints(10)
        tfEmail.addTarget(self, action: #selector(SearchDidChange(_:)), for: .editingChanged)
        btnCheck.isSelected = true
        btnLetsGo.isSelected = false
        btnLetsGo.isEnabled = false
        lblName.text = "Nice to meet you " + userName + "!"
     
        
      //  if let myImage = UIImage(named: "email"){
      //      tfEmail.withImage(direction: .Left, image: myImage, colorSeparator: //UIColor.green, colorBorder: UIColor.red)
      //  }
        lblMotivational.text = "Please send me motivational emails.\nDon’t worry, you can unsubscribe anytime."
        btnLetsGo.isSelected = false
        btnLetsGo.isEnabled = false
        tfEmail.setLeftView(image: UIImage.init(named: "email")!)
    }
    
    
    @objc func SearchDidChange(_ textField: UITextField) {
      
        if self.isValidEmail(candidate: tfEmail.text!) {
            btnLetsGo.isSelected = true
            btnLetsGo.isEnabled = true
        }else{
            btnLetsGo.isSelected = false
            btnLetsGo.isEnabled = false
        }
     
    }
   
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func actionLetsGo(_ sender: Any) {
        if self.isValidEmail(candidate: tfEmail.text!) {
            var parameter = [String : Any]()
             parameter[params.kfull_name] = userName
             parameter[params.kemail] = tfEmail.text
             parameter[params.kdevice_type] = "Ios"
             parameter[params.kios_token] = appDelegate.fcmToken
            if btnCheck.isSelected{
                parameter[params.kis_email_permission] = "1"
            }else{
                parameter[params.kis_email_permission] = "0"
            }
            
            
             ServerManager.shared.POST(url: ApiAction.signup  , param: parameter, header: nil) { (data, error) in
                 guard let data = data else {
                     print("data not available")
                     return
                 }
                 guard  let obj = try? JSONDecoder().decode(LoginModel.self, from: data) else {
                     return
                 }
                 if obj.code == ResponseApis.KSuccess {
                     UserDefaults.standard.setLoggedIn(value: true)
                     AppDataHelper.shard.logins = obj.userinfo
                     UserDefaults.standard.setUserData(value: data)
                    SwitchNav.homeRootNavigation()
                  /* //  DispatchQueue.main.async(execute: {
                     let _ = presentAlertWithOptions(kAppName, message: obj.message ?? "", controller: self, buttons: ["Ok"]) { (alert, actionTag) in
                                               if actionTag == 0 {
                                                   //Ok Button
                                                   SwitchNav.homeRootNavigation()
                                               } else {
                                                   //Cancel Button
                                               }
                                           }
                                      // })*/

                     
                 } else {
                     presentAlert(kAppName, msgStr: obj.message, controller: self)
                 }
                 
             }
            
         }
        
    }

    @IBAction func actionCheckUnCheck(_ sender: Any) {
        if btnCheck.isSelected == false {
            btnCheck.isSelected = true
        }else{
            btnCheck.isSelected = false
            }
    }
}

class AppDataHelper {
    static let shard = AppDataHelper()
    var logins: Userinfo!
    
}




extension UITextField {
  func setLeftView(image: UIImage) {
    let iconView = UIImageView(frame: CGRect(x: 15, y: 15, width: 18, height: 14)) // set your Own size
    iconView.image = image
    let iconContainerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 45, height: 45))
    iconContainerView.addSubview(iconView)
    
    
    leftView = iconContainerView
    leftViewMode = .always
   // self.tintColor = .lightGray
  }
}

