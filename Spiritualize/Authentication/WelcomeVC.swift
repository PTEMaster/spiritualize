//
//  ViewController.swift
//  Spiritualize
//
//  Created by mac on 10/12/20.
//

import UIKit

class WelcomeVC: KBaseViewController {
    @IBOutlet weak var lblmsg: UILabel!
    
    override func viewDidLoad()  {
        super.viewDidLoad()
//        for family: String in UIFont.familyNames
//           {
//               print("\(family)\n")
//               for names: String in UIFont.fontNames(forFamilyName: family)
//               {
//                   print("== \(names)")
//               }
//           }
        
        print(UserDefaults.standard.isLoggedIn())
        if UserDefaults.standard.isLoggedIn() {
            let data = UserDefaults.standard.getUserData()
            guard  let obj = try? JSONDecoder().decode(LoginModel.self, from: data) else {
                return
            }
            AppDataHelper.shard.logins = obj.userinfo
            SwitchNav.homeRootNavigation()
        }else{
           // SwitchNav.loginRootNavigation()
        }
        
        lblmsg.addCharacterSpacing()
    }
    
    @IBAction func actionGetStarted(_ sender: Any) {
        let vc = Signup1VC.instance(storyBoard: .main) as! Signup1VC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    


}



extension UIApplication {

    class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {

        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)

        } else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return getTopViewController(base: selected)

        } else if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
}



extension UIViewController {
    func topMostViewController() -> UIViewController {

        if let presented = self.presentedViewController {
            return presented.topMostViewController()
        }

        if let navigation = self as? UINavigationController {
            return navigation.visibleViewController?.topMostViewController() ?? navigation
        }

        if let tab = self as? UITabBarController {
            return tab.selectedViewController?.topMostViewController() ?? tab
    }

        return self
    }
}

extension UIApplication {
    func topMostViewController() -> UIViewController? {
        return self.keyWindow?.rootViewController?.topMostViewController()
    }
}



extension UILabel {
  func addCharacterSpacing(kernValue: Double = -0.7) {
    if let labelText = text, labelText.count > 0 {
      let attributedString = NSMutableAttributedString(string: labelText)
        attributedString.addAttribute(NSAttributedString.Key.kern, value: kernValue, range: NSRange(location: 0, length: attributedString.length - 1))
      attributedText = attributedString
    }
  }
}

