//
//  CGAppUtility.swift


import Foundation
import UIKit
import UserNotifications

let showLog = true

func presentAlert(_ titleStr : String?,msgStr : String?,controller : AnyObject?){
    DispatchQueue.main.async {
        let alert=UIAlertController(title: titleStr, message: msgStr, preferredStyle: UIAlertController.Style.alert);
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil));        
        //event handler with closure
        controller!.present(alert, animated: true, completion: nil);
    }
}

func presentAlertWithOptions(_ title: String, message: String,controller : AnyObject, buttons:[String], tapBlock:((UIAlertAction,Int) -> Void)?) -> UIAlertController {
    let controller =    UIApplication.shared.topMostViewController()
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert, buttons: buttons, tapBlock: tapBlock)
    DispatchQueue.main.async(execute: {
        controller?.present(alert, animated: true, completion: nil)
    })
    return alert
    
}



private extension UIAlertController {
    convenience init(title: String?, message: String?, preferredStyle: UIAlertController.Style, buttons:[String], tapBlock:((UIAlertAction,Int) -> Void)?) {
        self.init(title: title, message: message, preferredStyle:preferredStyle)
        var buttonIndex = 0
        for buttonTitle in buttons {
            let action = UIAlertAction(title: buttonTitle, preferredStyle: .default, buttonIndex: buttonIndex, tapBlock: tapBlock)
            buttonIndex += 1
            self.addAction(action)
        }
    }
    

}

extension UIAlertController {
    
    static func show(OnVc vc: UIViewController? = UIApplication.shared.keyWindow?.rootViewController, withTitle title : String, withDesc desc : String, withButtons btns : [UIAlertAction] = []) {
        
        DispatchQueue.main.async { [weak vc] in
            
            let ac = UIAlertController.init(title: title, message: desc, preferredStyle: .alert)
            
            for btn in btns {
                ac.addAction(btn)
            }
            
            vc?.present(ac, animated: true, completion: nil)
        }
    }
    
    
}

private extension UIAlertAction {
    convenience init(title: String?, preferredStyle: UIAlertAction.Style, buttonIndex:Int, tapBlock:((UIAlertAction,Int) -> Void)?) {        
        self.init(title: title, style: preferredStyle) {
            (action:UIAlertAction) in
            if let block = tapBlock {
                block(action,buttonIndex)
            }
        }
    }
}

func RGBA(_ r:CGFloat, g:CGFloat, b:CGFloat, a:CGFloat) -> UIColor {
    return UIColor(red: (r/255.0), green: (g/255.0), blue: (b/255.0), alpha: a)
}


/// Add tool bar on keyboard
///
/// - Parameters:
///   - textField: textfield on which we want toolbar
///   - target: where we show toolbar
/// - Returns: toolbar with buttons and their actions
func addToolBarOnTextfield(textField: UITextField, target: UIViewController) -> UIToolbar {    
    let numberToolbar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
    numberToolbar.barStyle = UIBarStyle.default
    numberToolbar.items = [
        UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil),
        UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: target, action: Selector(("doneWithNumberPad")))
    ]
    numberToolbar.sizeToFit()
    return numberToolbar
}

func doneWithNumberPad() {
}

// custom log
func logInfo(_ message: String, file: String = #file, function: String = #function, line: Int = #line, column: Int = #column) {
    if (showLog) {
        print("\(function): \(line): \(message)")
    }
}

//MARK: - Create notification

/// Create Local notification to show accident alert
///
/// - Parameters:
///   - titleString: Title string to show on alert
///   - messageToShow: message to show on alert
///   - userInfo: other information
///   - timeInterval: after given timeInterval no
///   - identifier: identifier to differentiate notification if needed

func createLocalNotification(_ titleString: String, messageToShow:String, userInfo:[AnyHashable: Any], timeInterval:Int, identifier:String) {
    if #available(iOS 10.0, *) {
        let content = UNMutableNotificationContent()
        content.title = titleString
        content.body = NSString.localizedUserNotificationString(forKey:
            messageToShow, arguments: nil)
        content.userInfo = userInfo
        content.sound = UNNotificationSound.default
        content.badge = 1
        content.categoryIdentifier = "yes.category"
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: TimeInterval(timeInterval),                                                      repeats: false)
        // Schedule the notification.
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests()
        center.add(request, withCompletionHandler: nil)
    } else {
        //Sysmetis code for ios 9
        // Fallback on earlier versions
        let notification = UILocalNotification()
        notification.alertTitle = titleString
        notification.alertBody = messageToShow
        notification.userInfo = userInfo
        notification.soundName = UILocalNotificationDefaultSoundName
        notification.applicationIconBadgeNumber = 1
        notification.category = "yes.category"
        notification.fireDate = NSDate(timeIntervalSinceNow:TimeInterval(timeInterval)) as Date
        UIApplication.shared.cancelAllLocalNotifications()
        UIApplication.shared.scheduledLocalNotifications = [notification]
    }
}

func removeAllNotification()
{
    if #available(iOS 10.0, *)
    {
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests()
    }
    else
    {
        UIApplication.shared.cancelAllLocalNotifications()
    }
}

//MARK: - call on any number
///
/// - Parameter number: get number for calling
func call(number:String){
    if let url = URL(string: "telprompt://\(number)"), UIApplication.shared.canOpenURL(url) {
        if #available(iOS 10, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
}

public extension UIWindow {
    /** @return Returns the current Top Most ViewController in hierarchy.   */
    func topMostControllerOnWindow()->UIViewController? {
        var topController = rootViewController
        while let presentedController = topController?.presentedViewController {
            topController = presentedController
        }
        return topController
    }
    
    /** @return Returns the topViewController in stack of topMostController.    */
    func currentViewController()->UIViewController? {
        var currentViewController = topMostControllerOnWindow()
        while currentViewController != nil && currentViewController is UINavigationController && (currentViewController as! UINavigationController).topViewController != nil {
            currentViewController = (currentViewController as! UINavigationController).topViewController
        }
        return currentViewController
    }
}


//MARK: - formated date and timestamp methods
func getDateFromString(strTimestamp: String) -> String {
    
    let dateFormatter = DateFormatter()
    if strTimestamp.count == 0 {
        return ""
    }else{
        let fromDate = Date.init(timeIntervalSince1970: (Double(strTimestamp)!))
        dateFormatter.dateFormat = "MMM dd/yyyy, hh:mm a"
        return dateFormatter.string(from: fromDate)
    }
}

func getUSFormatedDateFromSelectedDate(selectedDate: Date) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd-MM-yyyy"
    return dateFormatter.string(from: selectedDate)
}

func getTimeStringIn24HRS(selectedDate: Date) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "HH:mm"
    return dateFormatter.string(from: selectedDate)
}


func getDateStringInFormat(selectedDate: Date) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "HH:mm"
    return dateFormatter.string(from: selectedDate)
}

func getServerDateFromSelectedDate(selectedDate: Date) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    return dateFormatter.string(from: selectedDate)
}

func getDifferenceBetweenGivenDateWithCurrentDateInYears(selectedDate: String) -> String{
    
    if selectedDate.count == 0 {
        return ""
    } else {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        if let givenDate : Date = dateFormatter.date(from: selectedDate)
        {
            let calendar = Calendar.current
            let date1 = calendar.startOfDay(for: givenDate)
            let date2 = calendar.startOfDay(for: Date())
            let dateComponent = calendar.dateComponents([.year,.month], from: date1, to: date2)
            return "\(dateComponent.year ?? 0)"
        }
        else
        {
            return ""
        }
    }
}





public func getStingFromDict (_ dict:[String: Any], _ key:String) -> String {
    if let title = dict[key] as? String {
        return "\(title)"
    } else if let title = dict[key] as? NSNumber {
        return "\(title)"
    } else {
        return ""
    }
}


 func showAlertWithCallback(_ title: String?, message: String?, isWithCancel: Bool, handler: (() -> Void)? = nil) {
        /*if UIApplication.topViewController() != nil {
            if (UIApplication.topViewController()!.isKind(of: UIAlertController.self)) {
                return
            }
        }*/
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if isWithCancel {
            alertController.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        }
        
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            handler?()
        }))
        
        appDelegate.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
