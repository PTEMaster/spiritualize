//
//  ServerManger.swift
//  Eatsy
//
//  Created by Apple on 01/10/19.
//  Copyright © 2019 Rajesh Shinde. All rights reserved.
//

import UIKit
import Alamofire

typealias CompletionHandler = (_ resposnObj: AFDataResponse<Any>) -> Void
typealias JSONHandler = (_ responseJSON: [String: Any]) -> Void
typealias completionHandlerWithError = (_ responseJson: Data?, _ error: Error?) -> Void

class ServerManager {
    static let shared = ServerManager()
    
    private  init() {
        
    }
    //MARK:- Post Method with header with body
  
    func POST(url: String, param: Parameters,_ ShowLoader : Bool? = true , header: HTTPHeaders?, responseBlock: @escaping completionHandlerWithError){

        if !ReachabilityTraydi.isConnectedToNetwork() {

            DispatchQueue.main.async(execute: {
               // Util.showAlertWithCallback("", message: Message.msgCheckConnection, isWithCancel: false)
              //  let topVC2 =    UIApplication.shared.topMostViewController()
                Util.show(message: Message.networkAlertMessage)
            })
            return
        }
        if  ShowLoader!{
            Loader.showLoader()
        }
      
        AF.upload(multipartFormData: { multipartFormData in
            for (key, value) in param {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
        }, to: baseUrl +  url, method: .post , headers: nil).responseJSON { (response) in
            if  ShowLoader!{
            Loader.hideLoader()
            }
            switch response.result {
            
            case .success(let upload):
                print("\n\n------------------------------------")
                print("API:- \(baseUrl +  url)")
                print("Param:- \(param)")
                print("\nresponse:- \(upload)")
                print("--------------------------------------\n\n")
                responseBlock(response.data, nil )
                
            case .failure(let error):
                print("\n\n---------------Error-------------")
                print("API:- \(url)")
                print("Param:- \(param)")
                print("\nError:- \(error)")
                print("--------------------------------------\n\n")
                showAlertWithCallback(kAppName, message: error.localizedDescription, isWithCancel: false)
                responseBlock(nil, error )
            }
        }
        
    }

    //MARK:- Get Method with header with body
     func GET(url: String, param: Parameters, header: HTTPHeaders?, responseBlock:@escaping completionHandlerWithError){
        
        if !ReachabilityTraydi.isConnectedToNetwork() {

           // let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Please check your internet connection"])
            DispatchQueue.main.async(execute: {
             //   Util.showAlertWithCallback("", message: Message.msgCheckConnection, isWithCancel: false)
              //  let topVC2 =    UIApplication.shared.topMostViewController()
                Util.show(message: Message.networkAlertMessage)
            })
            
            return
          
        }
        
        Loader.showLoader()
        AF.upload(multipartFormData: { multipartFormData in
            for (key, value) in param {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
        }, to:baseUrl + url, method: .get , headers: header).responseJSON { (response) in
   
            Loader.hideLoader()
            switch response.result {
            
            case .success(let upload):
                print("\n\n------------------------------------")
                print("API:- \( baseUrl + url)")
                print("Param:- \(param)")
                print("\nresponse:- \(upload)")
                print("--------------------------------------\n\n")
                responseBlock(response.data, nil)
            case .failure(let error):
                print("\n\n---------------Error-------------")
                print("API:- \( baseUrl + url)")
                print("Param:- \(param)")
                print("\nError:- \(error)")
                print("--------------------------------------\n\n")
                responseBlock(nil, error )
            }
        }
        
    }
    //MARK:- POST With Image
     func POSTWithImage(url: String, param: Parameters, imageView: UIImageView, header: HTTPHeaders, responseBlock:@escaping completionHandlerWithError){
        

        let image = imageView.image?.jpegData(compressionQuality: 0.3)

        if !ReachabilityTraydi.isConnectedToNetwork() {
            
            DispatchQueue.main.async(execute: {
             //   Util.showAlertWithCallback("", message: Message.msgCheckConnection, isWithCancel: false)
                Util.show(message: Message.networkAlertMessage)
            })
            return
        }
        Loader.showLoader()
        AF.upload(multipartFormData: { multipartFormData in
            for (key, value) in param {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            multipartFormData.append(image!, withName: "image",fileName: "file.jpg", mimeType: "image/jpg")
            
        }, to: url, method: .post , headers: header).responseJSON { (response) in
            
            Loader.hideLoader()
            switch response.result {
            
            case .success(let upload):
                print("\n\n------------------------------------")
                print("API:- \(url)")
                print("Param:- \(param)")
                print("\nresponse:- \(upload)")
                print("--------------------------------------\n\n")
                responseBlock(response.data, nil)
                
            case .failure(let error):
                print("\n\n---------------Error-------------")
                print("API:- \(url)")
                print("Param:- \(param)")
                print("\nError:- \(error)")
                print("--------------------------------------\n\n")
                responseBlock(nil, error)
            }
        }
    }
}


struct ResponseApis {
    static let KSuccess = "1"
    static let kError = "0"
}



